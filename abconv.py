#!/usr/bin/env python3
#
# Heavily inspired by:
# - https://github.com/ckuethe/aax2mp3_py
# - https://gist.github.com/dcondrey/469e2850e7f88ac198e8c3ff111bda7c
#


import os
import re
import argparse
import json
import subprocess as sp

args = None

PLAIN_ALBUM_TEMPLATE = "{book}"
SERIES_ALBUM_TEMPLATE = "{series} {instalment} - {book}"

PLAIN_OUTDIR_TEMPLATE = "out/{author}/{album}"
SERIES_OUTDIR_TEMPLATE = "out/{author}/{series}/{instalment} - {book}"

FORMATS = {
    'mp3': {
        'settings': ['-c:a', 'libmp3lame', '-b:a', '256k'],
        'extension': 'mp3'
    }
}

TRIM_ARGS_TEMPLATES = {
    'intro': [
        'silence', '1', '0.1', '0.1%',
        'trim', '{trim_amount}',
        'silence', '1', '0.1', '0.1%',
        'pad', '{pad_amount}'
    ],
    'extro': [
        'reverse',
        'silence', '1', '0.1', '0.1%',
        'trim', '{trim_amount}',
        'silence', '1', '0.1', '0.1%',
        'pad', '{pad_amount}',
        'reverse'
    ]
}

def validate_args(args):
    if bool(args.series) is not bool(args.instalment):
        raise Exception("Either both a series and an instalment or neither of them must be specified!")


def validate_authcode(args):
    '''Ensure that an authcode is available'''
    
    if args.authcode:
        return

    authcode = os.environ.get('AUTHCODE', None)
    if authcode:
        args.authcode = authcode
        return

    for f in ['.authcode', '~/.authcode']:
        f = os.path.expanduser(f)
        if os.path.exists(f):
            with open(f) as fd:
                args.authcode = fd.read().strip()
                return 
    raise Exception("No authcode found in '.authcode', '~/.authcode', as '$AUTHCODE' environement variable or as command line argument '-a'!")


def validate_tools():
    '''Ensure that various dependencies are available'''

    required_tools = ['ffmpeg', 'ffprobe']
    required_str = ' '.join(required_tools)
    found = None
    with os.popen('which %s' % required_str) as fd:
        found = fd.read()

    for tool in required_tools:
        if tool not in found:
            raise Exception("Required tool %s not found in $PATH!" % tool)


def probe_metadata(source):
    '''Get source file metadata, eg. chapters, titles, codecs.'''

    if not os.path.exists(source):
        raise Exception("Source file '%s' not found!" % source)

    command = [
        'ffprobe', 
        '-v', 'error', 
        '-i', source, 
        '-of', 'json', 
        '-show_chapters', 
        '-show_programs', 
        '-show_format',]

    p = sp.run(command, stdout=sp.PIPE)
    buf = p.stdout.decode()
    return buf


def print_metadata(source):
    print(probe_metadata(source))


def get_metadata(source):
    return json.loads(probe_metadata(source))


def get_tags(metadata):
    return metadata['format']['tags']


def get_book(metadata, args):
    tags = get_tags(metadata)
    book = {
        'author': args.author or tags['artist'],
        'series': args.series,
        'instalment': args.instalment,
        'book': args.book or tags['title'],
        'year': args.year,
        'performer': args.performer
    }
    book['artist'] = args.artist_template.format_map(book)
    
    album_template = args.album_template or (
        SERIES_ALBUM_TEMPLATE if book['series'] else PLAIN_ALBUM_TEMPLATE
    )
    book['album'] = album_template.format_map(book)
    
    return book


def get_chapters(metadata, book, args):
    chapters = []
    chapters_count = len(metadata['chapters'])
    chapter_number_template = "{{:0{}d}}".format(len(str(chapters_count)))
    for i in range(chapters_count):
        chapter_data = metadata['chapters'][i]
        chapter = {
            'author': book['artist'],
            'series': book['series'],
            'instalment': book['instalment'],
            'book': book['book'],
            'year': book['year'],
            'album': book['album'],
            'performer': book['performer'],
            'chapter_number': i + 1,
            'formatted_chapter_number': chapter_number_template.format(i + 1),
            'chapters_count': chapters_count,
            'start_time': chapter_data['start_time'],
            'end_time': chapter_data['end_time'],
            'trim_intro': args.trim_intro if i == 0 else 0,
            'trim_extro': args.trim_extro if i == chapters_count - 1 else 0,
            'pad_with_silence': args.pad_with_silence
        }
        chapter['chapter_name'] = args.track_template.format_map(chapter)

        outdir_template = args.outdir_template or (
            SERIES_OUTDIR_TEMPLATE if book['series'] else PLAIN_OUTDIR_TEMPLATE
        )
        chapter['outdir'] = outdir_template.format_map(chapter)
    
        chapter['out'] = os.path.join(chapter['outdir'], "{}.{}".format(args.filename_template.format_map(chapter), FORMATS[args.format]['extension']))
        chapter['cover_image'] = os.path.join(chapter['outdir'], args.cover_image)
        chapters.append(chapter)
    return chapters


def extract_cover_image(source, cover_image):
    command = [
        "ffmpeg", '-y',
        '-i', source,
        '-map', '0:2',
        cover_image
    ]
    sp.run(command, stdout=sp.DEVNULL, stderr=sp.DEVNULL)


def get_read_source_command(source, authcode, chapter):
    read_source_command = [
        "ffmpeg",
        '-activation_bytes', authcode, 
        '-i', source,
        '-ss', str(chapter['start_time']),
        '-to', str(chapter['end_time']),
        '-f', 'wav',
        '-'
    ]
    return read_source_command


def get_trim_args(trim_args_templates, trim_amount, pad_amount):
    trim_args = []
    for template in trim_args_templates:
        trim_args.append(template.format(trim_amount=trim_amount, pad_amount=pad_amount))
    return trim_args


def get_process_performer_command(chapter):
    process_performer_command = [
        'sox', 
        '-t', 'wav',
        '-',
        '-t', 'wav',
        '-'
    ]
    if chapter['trim_intro']:
        process_performer_command.extend(get_trim_args(TRIM_ARGS_TEMPLATES['intro'], chapter['trim_intro'], chapter['pad_with_silence']))
    if chapter['trim_extro']:
        process_performer_command.extend(get_trim_args(TRIM_ARGS_TEMPLATES['extro'], chapter['trim_extro'], chapter['pad_with_silence']))
    return process_performer_command


def get_convert_and_write_command(source, chapter, format):
    convert_and_write_command = [
        "ffmpeg", '-y',
        '-i', '-',
        '-i', source,
        '-map', '0:0',
        '-map', '1:2',
        '-map_chapters', '-1',
        '-map_metadata', '-1',
        '-metadata', u'artist={}'.format(chapter['author']),
        '-metadata', u'album_artist={}'.format(chapter['author']),
        '-metadata', u'album={}'.format(chapter['album']),
        '-metadata', u'title={}'.format(chapter['chapter_name']),
        '-metadata', u'track={}'.format(chapter['chapter_number']),
        '-metadata:s:v', 'title=Album cover',
        '-metadata:s:v', 'performer=Cover (Front)',
        '-id3v2_version', '4',
        '-write_id3v1', '1'
    ]
    if chapter['year']:
        convert_and_write_command.extend(['-metadata', "date={}".format(chapter['year'])])
    if chapter['performer']:
        convert_and_write_command.extend(['-metadata', "performer={}".format(chapter['performer'])])
    convert_and_write_command.extend(FORMATS[format]['settings'])
    convert_and_write_command.append(chapter['out'])
    return convert_and_write_command

def convert_chapter(source, authcode, chapter, format):
    print("Converting chapter {chapter_number} to '{out}'...".format_map(chapter))

    if not os.path.isdir(chapter['outdir']):
        os.makedirs(chapter['outdir'])

    if not os.path.isfile(chapter['cover_image']):
        extract_cover_image(source, chapter['cover_image'])

    read_source = sp.Popen(get_read_source_command(source, authcode, chapter), stdout=sp.PIPE)
    process_performer = sp.Popen(get_process_performer_command(chapter), stdin=read_source.stdout, stdout=sp.PIPE)
    rt = sp.call(get_convert_and_write_command(source, chapter, format), stdin=process_performer.stdout)


def convert_chapters(source, authcode, chapters, format):
    for chapter in chapters:
        convert_chapter(source, authcode, chapter, format)


def main():
    global args

    ap = argparse.ArgumentParser(
        description="Convert aax audiobooks to collections of audio files.",
        epilog="Requires ffmpeg to be installed and in path."
    )

    ap.add_argument('-x', '--extract-metadata', default=False, dest='extract_metadata', action='store_true', help='Only extract metadata.')

    ap.add_argument('-c', '--authcode', default=None, dest='authcode', help='Authorization code.')

    ap.add_argument('-a', '--author', default=None, dest='author', help='Author name.')
    ap.add_argument('-b', '--book', default=None, dest='book', help='Book title.')
    ap.add_argument('-s', '--series', default=None, dest='series', help='Series name.')
    ap.add_argument('-i', '--instalment', default=None, dest='instalment', help='Instalment number within series.')

    ap.add_argument('-A', '--artist-template', default='{author}', dest='artist_template', help='Artist tag template. ["%(default)s"]')
    ap.add_argument('-B', '--album-template', default=None, dest='album_template', help='Album tag template. ["{} for series, "{}" else]'.format(SERIES_ALBUM_TEMPLATE, PLAIN_ALBUM_TEMPLATE))
    ap.add_argument('-T', '--track-template', default='{album} {chapter_number}/{chapters_count}', dest='track_template', help='Track tag template. ["%(default)s"]')
    ap.add_argument('-p', '--performer', default=None, dest='performer', help='Performer tag content.')
    ap.add_argument('-y', '--year', default=None, dest='year', help='Year.')
    
    ap.add_argument('-o', '--outdir-template', default=None, dest='outdir_template', help='Output directory template. ["{}" for series, "{}" else]'.format(SERIES_OUTDIR_TEMPLATE, PLAIN_OUTDIR_TEMPLATE))
    ap.add_argument('-n', '--filename-template', default='{formatted_chapter_number} - {book} {chapter_number}', dest='filename_template', help='Track filename template. ["%(default)s"]')
    
    ap.add_argument('-f', '--format', default='mp3', choices=FORMATS.keys(), dest='format', help='Output format. ["%(default)s"]')
    ap.add_argument('-t', '--trim-intro', default=None, dest='trim_intro', type=float, help='After removing Trim start of first track by specified number of seconds. Try 1.5 for English and 3.5 for German audio books.')
    ap.add_argument('-u', '--trim-extro', default=None, dest='trim_extro', type=float, help='Trim end of last track by specified number of seconds. Try 2.5 for both English and German audio books.')
    ap.add_argument('-w', '--pad-with-silence', default=1.0, dest='pad_with_silence', type=float, help='After trimming, pad beginning of first chapter and end of last chapter, respectively, with the specified number of seconds of silence. [%(default)s]')


    ap.add_argument('-m', '--cover-image', default='album.jpg', dest='cover_image', help='Cover image filename. ["%(default)s"]')

    ap.add_argument(dest='source')
    
    args = ap.parse_args()

    validate_args(args)
    validate_tools()

    if args.extract_metadata:
        print_metadata(args.source)
    else:
        validate_authcode(args)
        metadata = get_metadata(args.source)
        book = get_book(metadata, args)
        chapters = get_chapters(metadata, book, args)
        convert_chapters(args.source, args.authcode, chapters, args.format)


if __name__ == '__main__':
    main()
 